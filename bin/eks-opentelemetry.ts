#!/usr/bin/env node
import 'source-map-support/register';
import * as cdk from '@aws-cdk/core';
import { EksOpentelemetryStack } from '../lib/eks-opentelemetry-stack';

import * as dotenv from "dotenv";

dotenv.config();

const account = process.env.account;
const region = process.env.region;

const app_env = {
    region: region,
    account: account
}


const app = new cdk.App();
new EksOpentelemetryStack(app, 'EksOpentelemetryStack', {env: app_env});
