import {App, Chart} from 'cdk8s';
import {Construct} from 'constructs';
import {
    Deployment
    
} from "../imports/k8s";

interface OpenTelemetryChartProps {
    region: string,
    namespace: string
    serviceAccountName: string
}


export class OpenTelemetryChart extends Chart {
    constructor(scope: Construct, name: string, props: OpenTelemetryChartProps) {
        super(scope, name);


        /*
        https://github.com/aws-observability/aws-otel-collector/blob/main/examples/eks/eks-sidecar.yaml
        */

        const myDeployment = new Deployment(this, 'aws-otel', {
            metadata: {
                namespace: props.namespace
            },
            spec: {
                replicas: 1,
                selector: {
                    matchLabels: {
                        "name": "aws-otel-eks-sidecar"
                    }
                },
                template: {
                    metadata: {
                        labels: {
                            "name": "aws-otel-eks-sidecar"
                        }
                    },
                    spec: {
                        serviceAccountName: props.serviceAccountName,
                        containers: [
                            {
                                name: "aws-otel-emitter",
                                image: "aottestbed/aws-otel-collector-sample-app:java-0.1.0",
                                imagePullPolicy: "Always",
                                env: [
                                    {
                                        name: "OTEL_OTLP_ENDPOINT",
                                        value: "localhost:55680"
                                    },
                                    {
                                        name: "OTEL_RESOURCE_ATTRIBUTES",
                                        value: "service.namespace=AWSObservability,service.name=CloudWatchEKSService"
                                    },
                                    {
                                        name: "S3_REGION",
                                        value: props.region
                                    }

                                ]

                                
                            },
                            {
                                name: "aws-otel-collector",
                                image: "amazon/aws-otel-collector:latest",
                                imagePullPolicy: "Always",
                                env: [
                                    {
                                        name: "AWS_REGION",
                                        value: props.region
                                    }

                                ],
                                resources: {
                                    limits: {
                                        cpu: "256m",
                                        memory: "512Mi"
                                    },
                                    requests: {
                                        cpu: "32m",
                                        memory: "24Mi"
                                    }
                                }

                                
                            },
                            
                        ],
                        
                    }
                }
            }

        })
    }
}