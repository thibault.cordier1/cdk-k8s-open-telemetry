
import * as ec2 from '@aws-cdk/aws-ec2';
import * as eks from '@aws-cdk/aws-eks';
import * as cdk from '@aws-cdk/core';
import * as iam from '@aws-cdk/aws-iam';

import * as cdk8s from 'cdk8s';
import { PolicyStatement } from '@aws-cdk/aws-iam';
import { OpenTelemetryChart } from './opentelemetry-chart';
import { Stack } from '@aws-cdk/core';

export class EksOpentelemetryStack extends cdk.Stack {
  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    // The code that defines your stack goes here

    const myVPC = new ec2.Vpc(this, "myVPC", {

      cidr: "10.21.0.0/16",
      maxAzs: 2,
      natGateways: 1,
      subnetConfiguration: [
        {
          name: "ECS-Public-Subnet",
          subnetType: ec2.SubnetType.PUBLIC
        },
        {
          name: "ECS-Nodes-Subnet",
          subnetType: ec2.SubnetType.PRIVATE
        }
      ],
    })

    const nodesSecurityGroup = new ec2.SecurityGroup(this, 'EKS-Node-SG', {
      vpc: myVPC,
      description: "Security Group to allow nodes to communicate",
      allowAllOutbound: true,

    })

    const nodeSecurityInboundRule = new ec2.CfnSecurityGroupIngress(this, 'NodeInbound', {
      ipProtocol: "-1",
      groupId: nodesSecurityGroup.securityGroupId,
      sourceSecurityGroupId: nodesSecurityGroup.securityGroupId
    })

    const clusterRole = new iam.Role(this, 'EKSClusterRole', {
      assumedBy: new iam.ServicePrincipal("eks.amazonaws.com"),
      managedPolicies: [
        iam.ManagedPolicy.fromAwsManagedPolicyName("AmazonEKSClusterPolicy")
      ]
    })


    const AdministratorsIamPolicy = new iam.Policy(this, 'EKS-ADMINISTRATOR-IAM-Policy', {
      statements: [
        new iam.PolicyStatement({
          effect: iam.Effect.ALLOW,
          resources: ["*"],
          actions: [
            "eks:DescribeCluster"
          ]
        })
      ]
    })


    const AdministratorRole = new iam.Role(this, 'AdministratorsOfEKSCluster', {
      assumedBy: new iam.AccountPrincipal(cdk.Stack.of(this).account),
    })
    AdministratorRole.attachInlinePolicy(AdministratorsIamPolicy)


    const myEKSCluster = new eks.Cluster(this, 'EKSCluster', {
      version: eks.KubernetesVersion.V1_18,
      mastersRole: AdministratorRole,
      role: clusterRole,
      securityGroup: nodesSecurityGroup,
      vpc: myVPC,

    })


    const openTelemetryPolicy = new iam.Policy(this, 'openTelemetryPolicy', {
      statements: [
        new PolicyStatement({
          effect: iam.Effect.ALLOW,
          actions: [
            "logs:PutLogEvents",
            "logs:CreateLogGroup",
            "logs:CreateLogStream",
            "logs:DescribeLogStreams",
            "logs:DescribeLogGroups",
            "xray:PutTraceSegments",
            "xray:PutTelemetryRecords",
            "xray:GetSamplingRules",
            "xray:GetSamplingTargets",
            "xray:GetSamplingStatisticSummaries",
            "ssm:GetParameters"
          ],
          resources: ["*"]
        })
      ]
    })


    const namespace = myEKSCluster.addManifest('telemetryNamespace', {
      apiVersion: "v1",
      kind: "Namespace",
      metadata: {
        name: 'telemetry',
        labels: {
          name: `telemetry`
        }
      }
    });


    const sa = myEKSCluster.addServiceAccount('aws-otel-collector', {
      namespace: "telemetry",
    })
    // Wait for namespace is created
    sa.node.addDependency(namespace)

    sa.role.attachInlinePolicy(openTelemetryPolicy)
    new cdk.CfnOutput(this, 'ServiceAccountIamRoleForTelemetry', { value: sa.role.roleArn })


    const myTelemetryChart = myEKSCluster.addCdk8sChart('telemetryApp', new OpenTelemetryChart(new cdk8s.App(), 'telemetryAppChart', {
      namespace: sa.serviceAccountNamespace,
      region: Stack.of(this).region,
      serviceAccountName: sa.serviceAccountName
    }))
    // Wait for namespace is created
    myTelemetryChart.node.addDependency(namespace)


  }
}
